<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('bomb_model');
        $this->load->model('player_model');
    }

	public function index()
	{
		$players = $this->db->get("Player");
        if( sizeof($players) > 0 ) {
           return json_encode($players);
        } else {
            return false;
        }
	}

    public function new_player() {
        $json = $this->input->post('json');
        $obj  = json_decode($json, true);

        $name = $obj['username'];        
        $lng  = $obj['current_lng'];
        $lat  = $obj['current_lat'];
        $res  = $this->player_model->get(array('username' => $name));
        if( $res == NULL || sizeof($res) == 0 ) {
            return false;
        } else {
            return $this->player_model->insert(array('username' => $name, 'current_lng' => $lng, 'current_lat' => $lat));
        }
    }

    public function update_players() {
        $json = $this->input->post('json');
        $obj  = json_decode($json, true);

        $lng  = $obj['current_lng'];
        $lat  = $obj['current_lat'];
        $id   = $obj['id'];
        return $this->player_model->update(array('current_lng' => $lng, 'current_lat' => $lat), array('id' => $id));
    }

    public function place_bomb() {
       $json    = $this->input->post('json');
       $obj     = json_decode($json, true);

       $lat     = $obj['current_lat'];
       $lng     = $obj['current_lng'];
       $user_id = $obj['user_id'];

       $this->bomb_model->insert(array('current_lng' => $lng, 'current_lat' => $lat, 'user_id' => $user_id));
    }

    public function get_bombs() {
       return json_encode($this->db->get('Bomb')->result_array); 
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
