CREATE TABLE `Player` (
    `id` INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` TEXT NOT NULL,
    `current_lng` FLOAT,
    `current_lat` FLOAT
);

CREATE TABLE `Bomb` (
    `id` INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id` INT(255) NOT NULL,
    `current_lng` FLOAT,
    `current_lat` FLOAT
);
