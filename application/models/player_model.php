<?php if(!defined("BASEPATH")) exit ('No direct script access allowed');

class Player_model extends CI_Model {

   public function __construct() {
        parent::__construct();
   }

   public function get($arr, $plural = false) {
        $query = $this->db->get_where('Player', $arr);
        if(!$plural) {
            $res = $query->row_array();
        } else {
            $res = $query->result_array();
        }

        return $res;
   }

   public function insert($arr) {
        if( $this->db->insert('Player', $arr)) {
            return true;
        } else {
            return false;
        }
   }

   public function update($arr, $where) {
        $query = $this->db->get_where('Player', $where);
        $res   = $query->row_array();

        if(sizeof($res) > 0) {
            if($this->db->update('Player', $arr, $where)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
   }
}
?>
